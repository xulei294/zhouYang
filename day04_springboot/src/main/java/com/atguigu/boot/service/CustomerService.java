
package com.atguigu.boot.service;

import com.atguigu.boot.entity.Customer;
import com.atguigu.boot.mapper.CustomerMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @auther zzyy
 * @create 2022-09-02 17:38
 */
@Service
@Slf4j
public class CustomerService
{
    @Autowired
    private CustomerMapper customerMapper;

    public void addCustomer(Customer customer){
        customerMapper.add(customer);
    }

    public List<Customer> list()
    {
        return customerMapper.list();
    }

    public Customer getCustomerById(Integer id)
    {
        return customerMapper.getCustomerById(id);
    }

    public void deleteCustomer(Integer id) {
        customerMapper.deleteCustomer(id);
    }

    public void updateCustomer(Customer customer) {
        customerMapper.updateMapper(customer);
    }
}


