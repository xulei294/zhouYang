package com.atguigu.boot.config;

import com.atguigu.boot.entity.MyBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * @className: MyBeanConfig
 * @description: TODO 类描述
 * @author: JJBoomxl
 * @date: 2022/09/13 19:43
 **/
@Import(SpringAnnotationConfig.class)
@Configuration
public class MyBeanConfig {

    @Bean
    public MyBean myBean(){
        return new MyBean(32,"mybean123");
    }
}
