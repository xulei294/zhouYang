package com.atguigu.boot.config;

import com.atguigu.boot.entity.Dog;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @className: SpringAnnotationConfig
 * @description: TODO 类描述
 * @author: JJBoomxl
 * @date: 2022/09/13 19:24
 **/
@Configuration
public class SpringAnnotationConfig {

    @Bean
    public Dog dog(){
        return new Dog(12,"柯基勾勾");
    }
}
