package com.atguigu.boot.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @className: Mybean
 * @description: TODO 类描述
 * @author: JJBoomxl
 * @date: 2022/09/13 19:40
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MyBean
{
    private Integer id;
    private String  beanName;
}
