package com.atguigu.boot.entity;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @className: Dog
 * @description: TODO 类描述
 * @author: JJBoomxl
 * @date: 2022/09/13 19:26
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@Api("勾勾类测试用")
public class Dog {

    @ApiModelProperty("狗子的id")
    private Integer id;
    @ApiModelProperty("小狗的名字")
    private String name;
}
