package com.atguigu.boot.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @auther zzyy
 * @create 2022-08-31 19:14
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Component
@ConfigurationProperties(prefix = "person")
public class Person
{
    private Integer id;
    private String  personName;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date birth;
    private boolean flag;

    private Dog dog;
    private Map map;

    private String[] itSkills;
    private List lists;
    private Set sets;
    private List<Dog> dogList;

}


