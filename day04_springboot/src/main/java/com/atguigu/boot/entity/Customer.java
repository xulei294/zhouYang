package com.atguigu.boot.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.Date;

/**
 * @className: Customer
 * @description: TODO 类描述
 * @author: JJBoomxl
 * @date: 2022/09/14 20:37
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@Component
public class Customer implements Serializable {

    private Integer id;

    private String cname;

    private Integer age;

    private String phone;

    private Byte sex;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date birth;
}
