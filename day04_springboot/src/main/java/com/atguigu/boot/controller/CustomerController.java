
package com.atguigu.boot.controller;

import com.atguigu.boot.entity.Customer;
import com.atguigu.boot.service.CustomerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @auther zzyy
 * @create 2022-09-02 17:38
 */
@RestController
@Slf4j
@Api(description = "customer接口")
public class CustomerController
{
    @Resource
    private CustomerService customerService;

    @ApiOperation("新增一个用户")
    @RequestMapping(value = "/customer/add", method = RequestMethod.POST)
    public void addCustomer(Customer customer) {

        customerService.addCustomer(customer);
    }

    @ApiOperation("根据id获取一个用户")
    @RequestMapping(value = "/customer/{id}", method = RequestMethod.GET)
    public Customer getCustomerById(@PathVariable Integer id) {
        return customerService.getCustomerById(id);
    }

    @ApiOperation("列出全部用户")
    @RequestMapping(value = "/customer/list", method = RequestMethod.GET)
    public List<Customer> list() {
        return customerService.list();
    }

    @ApiOperation("根据id删除用户")
    @RequestMapping(value = "/customer/delete/{id}",method = RequestMethod.GET)
    public String deleteCustomer(@PathVariable Integer id){
        customerService.deleteCustomer(id);
        return "OK";
    }

    @ApiOperation("更改用户")
    @RequestMapping(value = "/customer/update",method = RequestMethod.PUT)
    public String updateCustomer(Customer customer){
        customerService.updateCustomer(customer);
        return "update OK!!!";
    }

}


