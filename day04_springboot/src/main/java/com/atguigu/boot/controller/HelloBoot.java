package com.atguigu.boot.controller;

import com.atguigu.boot.entity.Dog;
import com.atguigu.boot.entity.MyBean;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @className: HelloBoot
 * @description: TODO 类描述
 * @author: JJBoomxl
 * @date: 2022/09/13 19:03
 **/
@RestController
@Slf4j
public class HelloBoot {

    @Autowired
    private Dog dog;

    @Autowired
    private MyBean myBean;

    @GetMapping("/helloBoot")
    public String helloBoot(){
        return "-----hello springboot";
    }

    @RequestMapping("/getDog")
    public Dog getDog(){
        log.debug("-----debug:{}", dog);
        log.debug("-----debug:{}", myBean);
        return dog;
    }
}
