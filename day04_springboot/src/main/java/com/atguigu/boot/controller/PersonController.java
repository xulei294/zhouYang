package com.atguigu.boot.controller;

import com.atguigu.boot.entity.Person;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

/**
 * @className: PersonController
 * @description: TODO 类描述
 * @author: JJBoomxl
 * @date: 2022/09/13 20:01
 **/

@RestController
@Slf4j
public class PersonController {

    @Value("${server.port}")
    private String port;

    @Autowired
    private Person person;

    @RequestMapping("/init")
    public String init() {
        log.debug("---port:{},person:{}", port,person);
        return "port:" + port + " " + person;
    }
}
