package com.atguigu.boot.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @className: RedisController
 * @description: TODO 类描述
 * @author: JJBoomxl
 * @date: 2022/09/14 19:33
 **/
@RestController
public class RedisController {

    @Autowired
    private RedisTemplate redisTemplate;

    @RequestMapping(value = "/redis/add",method = RequestMethod.POST)
    public String add(){
        redisTemplate.opsForValue().set("addvalue", "java0526");
        return "add OK!";
    }

    @RequestMapping(value = "/redis/{key}",method = RequestMethod.GET)
    public String getValue(@PathVariable String key){
        return (String) redisTemplate.opsForValue().get(key);
    }
}
