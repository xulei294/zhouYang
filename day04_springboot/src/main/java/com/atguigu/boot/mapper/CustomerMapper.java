package com.atguigu.boot.mapper;

import com.atguigu.boot.entity.Customer;

import java.util.List;

/**
 * @auther zzyy
 * @create 2022-09-02 17:26
 */
public interface CustomerMapper
{
    public void add(Customer customer);

    public List<Customer> list();
    public Customer getCustomerById(Integer id);

    public void deleteCustomer(Integer id);

    void updateMapper(Customer customer);
}


