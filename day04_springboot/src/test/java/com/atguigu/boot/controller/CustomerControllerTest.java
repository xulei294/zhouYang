package com.atguigu.boot.controller;

import com.atguigu.boot.service.CustomerService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class CustomerControllerTest {

    @Autowired
    private CustomerService customerService;
    @Test
    void addCustomer() {
        Customer customer = new Customer();
        customer.setCname("牛牛");
        customer.setAge(34);
        customer.setPhone("19011112222");
        customer.setSex((byte) 1);

        customerService.addCustomer(customer);

    }

    @Test
    void getCustomerById() {
    }

    @Test
    void list() {
        List<Customer> list = customerService.list();
        list.forEach(s -> System.out.println(s));
    }
}