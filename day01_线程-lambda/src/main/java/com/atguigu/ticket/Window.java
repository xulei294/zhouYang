package com.atguigu.ticket;

public class Window{
    public static void main(String[] args) {
        SaleTicketDemo saleTicketDemo = new SaleTicketDemo();
        new Thread(() -> {
            for (int i = 0; i < 101; i++) {
                saleTicketDemo.sale();
            }
        },"window1").start();

        new Thread(() -> {
            for (int i = 0; i < 101; i++) {
                saleTicketDemo.sale();
            }
        },"window2").start();
    }
}
