package com.atguigu.ticket;


import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
/**
 * @auther zzyy
 * @create 2022-04-25 23:11
 *  * 三个售票员    卖出      50张票
 *  * 如何编写企业级需要的工程化代码？ 多对1 多个线程操作同一个资源类
 *  JUC口诀
 *  OOP--面向对象编程
 *   1 你的需求是什么？空调制冷，点一下降低一个温度
 *
 *   2 你描述的需求我将它用一个方法搞定，告诉我，java一切皆对象
 *
 *   3 对象 = Field + method
 *
 *   4 方法  封装进入具体对象，这个对象就是被我们操作的资源类
 *
 *
 * 1 在高内聚低耦合前提下，线程  操作  资源类
 *
 */
public class SaleTicketDemo{
   private int tickets = 100;

   /*public synchronized void sale(){
       if (tickets > 0){
           String name = Thread.currentThread().getName();
           System.out.println("票号:" + tickets + '\t'+ name + "\t" + "卖出了一张票还剩" + --tickets);
       }
   }*/

    //独占锁+可重入锁+公平|默认非公平锁
   Lock lock = new ReentrantLock();


   public void sale(){
       String name = Thread.currentThread().getName();
       lock.lock();
       try {
           if (tickets > 0){
               System.out.println("票号:" + tickets + '\t'+ name + "\t" + "卖出了一张票还剩" + --tickets);
           }
       } finally {
           lock.unlock();
       }
   }


}






