package com.atguigu.ticket;


/**
 * Lambda Express
 * 前提是函数式接口
 *
 * 1    拷贝小括号，写死右箭头，落地大括号
 * 2    @FunctionalInterface显示的定义一个函数式接口，假如不写，只有一个方法隐式的定义为函数式接口
 * 3
 */
@FunctionalInterface
interface Foo{

    //void hello();

    int mul(int x, int y);

    //void show(String context);

    default int add(int x, int y){
        return x+ y;
    }

    static int sub(int a, int b){
        return a - b;
    }
}

public class LambdaDemo {

    public static void main(String[] args){

        /*Foo foo = new Foo() {
            @Override
            public void hello() {
                System.out.println("hello XAjava0526");
            }
        };*/

        Foo foo = (x,y) -> {return x*y;};

        //System.out.println(foo.mul(2, 3));

        //foo.hello();


        System.out.println(foo.add(1, 3));

        System.out.println(Foo.sub(10, 5));
    }
}
