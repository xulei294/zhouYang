package com.atguigu.ticket;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class SyncLockDemo {

    Lock lock = new ReentrantLock();

    public void m1(){
        String name = Thread.currentThread().getName();
        lock.lock();
        try {
            System.out.println(name + "-------" + "come in");
            try { TimeUnit.SECONDS.sleep(1); } catch (InterruptedException e) { e.printStackTrace(); }
            System.out.println(name + "-------" + "leaf out");
        } finally {
            lock.unlock();
        }
    }

    public void m2(){
        String name = Thread.currentThread().getName();

        if (lock.tryLock()) {
            try {
                System.out.println(name + "-------" + "come in");
                try {
                    TimeUnit.SECONDS.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(name + "-------" + "leaf out");
            } finally {
                lock.unlock();
            }
        }else{
            System.out.println(name + "不等待,直接离开");
        }
    }

    public void m3(){
        String name = Thread.currentThread().getName();

        try {
            if (lock.tryLock(3L,TimeUnit.SECONDS)) {
                try {
                    System.out.println(name + "-------" + "come in");
                    try {
                        TimeUnit.SECONDS.sleep(4);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println(name + "-------" + "leaf out");
                } finally {
                    lock.unlock();
                }
            }else{
                System.out.println(name + "不等待,超时离开");
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args){

        SyncLockDemo syncLockDemo = new SyncLockDemo();

        new Thread(() -> {
            syncLockDemo.m3();
        },"p1").start();

        new Thread(() -> {
            syncLockDemo.m3();
        },"p2").start();

    }
}
