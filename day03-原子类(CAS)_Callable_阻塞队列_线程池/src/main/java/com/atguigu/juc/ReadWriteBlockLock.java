package com.atguigu.juc;


import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

class MyCache{
    private Map<String,String> data = new HashMap<>();
    /*private Lock lock = new ReentrantLock();

    public void get(String key){
        lock.lock();
        try {
            System.out.println(Thread.currentThread().getName() + "开始取数据");
            String result = data.get(key);
            System.out.println(Thread.currentThread().getName() + "取结束 result:" + result);
        } finally {
            lock.unlock();
        }
    }

    public void save(String key,String value){
        lock.lock();
        try {
            System.out.println(Thread.currentThread().getName() + "开始存数据");
            data.put(key,value);
            System.out.println(Thread.currentThread().getName() + "存数据结束" );
        } finally {
            lock.unlock();
        }
    }*/

    ReentrantReadWriteLock rwLock = new ReentrantReadWriteLock();
    public void get(String key){
        rwLock.readLock().lock();
        try {
            System.out.println(Thread.currentThread().getName() + "开始取数据");
            String result = data.get(key);
            System.out.println(Thread.currentThread().getName() + "取结束 result:" + result);
        } finally {
            rwLock.readLock().unlock();
        }
    }

    public void save(String key,String value){
        rwLock.writeLock().lock();
        try {
            System.out.println(Thread.currentThread().getName() + "开始存数据");
            data.put(key,value);
            System.out.println(Thread.currentThread().getName() + "存数据结束" );
        } finally {
            rwLock.writeLock().unlock();
        }
    }
}
/**
 * @className: ReadWriteBlockLock
 * @description: TODO 类描述
 * @author: JJBoomxl
 * @date: 2022/09/08 20:05
 * 读写锁
 *
 * 对于同一个资源，我们涉及多线程的操作，有读，有写，交替进行。
 * 为了保证读写的数据一致性。
 *
 * 读 读 可共享
 * 读 写 不共享
 * 写 写 不共享
 * 读的时候希望高并发同时进行，可以共享，可以多个线程同时操作进行中.....
 * 写的时候为了保证数据一致性，需要独占排它。
 *
 *
 * 题目：5个线程读，5个线程写入，操作同一个资源
 *
 *
 * 1 不加锁                不可以，写的时候原子性被破坏
 * 2 加ReentrantLock锁     写控制了，但是没有并发度，并发性能不好
 * 3 加读写锁               规范写入，写唯一，读并发
 */
public class ReadWriteBlockLock {
    public static void main(String[] args) {

        MyCache myCache = new MyCache();

        for (int i = 0; i < 10; i++) {
            String finalI = i + "";
            new Thread(() -> {
                myCache.save(finalI, finalI);
            },finalI).start();
        }


        for (int i = 0; i < 10; i++) {
            String finalI = i + "";
            new Thread(() -> {
                myCache.get(finalI);
            },finalI).start();
        }
    }
}
