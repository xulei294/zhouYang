package com.atguigu.juc;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

class MyThread implements Callable {

    @Override
    public Object call() throws Exception {
        System.out.println(Thread.currentThread().getName() + "----- come in Callable进行复杂计算");
        return "welcome java0526";
    }
}

/**
 * @className: CallableDemo
 * @description: TODO 类描述
 * @author: JJBoomxl
 * @date: 2022/09/08 19:30
 **/
public class CallableDemo {
    public static void main(String[] args) throws ExecutionException, InterruptedException {

        FutureTask futureTask1 = new FutureTask<>(new MyThread());
        FutureTask futureTask2 = new FutureTask<>(new MyThread());


        Thread t1 = new Thread(futureTask1);
        t1.start();
        Thread t2 = new Thread(futureTask2);
        t2.start();

        System.out.println(futureTask1.get());
        System.out.println(futureTask2.get());

        new Thread(new FutureTask<>(() -> {
            System.out.println(Thread.currentThread().getName() + "----- come in Callable进行复杂计算");
            return "welcome java0526";
        }),"t1").start();

        new Thread(new FutureTask<>(() -> {
            System.out.println(Thread.currentThread().getName() + "----- come in Callable进行复杂计算");
            return "welcome java0526";
        }),"t2").start();

        System.out.println(Thread.currentThread().getName() + "task over");
    }
}
