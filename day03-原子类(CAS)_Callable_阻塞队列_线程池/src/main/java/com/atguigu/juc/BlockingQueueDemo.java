package com.atguigu.juc;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/**
 * @className: BlockingQueueDemo
 * @description: TODO 类描述
 * @author: JJBoomxl
 * @date: 2022/09/08 20:41
 *
 * add(e) remove() element()
 * 抛出异常	当阻塞队列满时，再往队列里add插入元素会抛IllegalStateException:Queue full
 * 当阻塞队列空时，再往队列里remove移除元素会抛NoSuchElementException
 *
 * offer(e) poll() peek()
 * 特殊值	插入方法，成功ture失败false
 * 移除方法，成功返回出队列的元素，队列里没有就返回null
 *
 * 一直阻塞	当阻塞队列满时，生产者线程继续往队列里put元素，队列会一直阻塞生产者线程直到put数据or响应中断退出
 * 当阻塞队列空时，消费者线程试图从队列里take元素，队列会一直阻塞消费者线程直到队列可用
 *
 * 超时退出	当阻塞队列满时，队列会阻塞生产者线程一定时间，超过限时后生产者线程会退出
 **/
public class BlockingQueueDemo {

    public static void main(String[] args) throws InterruptedException {

        BlockingQueue blockingQueue = new ArrayBlockingQueue(3);


        new Thread(() -> {
            try {
                blockingQueue.put("a");
                blockingQueue.put("b");
                blockingQueue.put("c");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            //System.out.println(blockingQueue.offer("x"));
        },"t1").start();


        /*new Thread(() -> {
            try { TimeUnit.SECONDS.sleep(2); } catch (InterruptedException e) { e.printStackTrace(); }
            System.out.println(blockingQueue.remove());
            System.out.println(blockingQueue.poll());
            System.out.println(blockingQueue.element());
            System.out.println(blockingQueue.peek());
        },"t2").start();*/

        //测试阻塞加入
        //blockingQueue.put("X");

        try { TimeUnit.SECONDS.sleep(3); } catch (InterruptedException e) { e.printStackTrace(); }
        //测试阻塞移除
        //blockingQueue.take();

        //测试超时插入
        System.out.println(blockingQueue.offer("x", 1, TimeUnit.SECONDS));

        //测试超时移除
        System.out.println(blockingQueue.poll( 4, TimeUnit.SECONDS));

        System.out.println(blockingQueue.poll());


    }
}
