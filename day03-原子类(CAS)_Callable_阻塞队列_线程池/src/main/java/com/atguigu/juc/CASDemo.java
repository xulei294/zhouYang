package com.atguigu.juc;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

class OperatNum{
    /*private int num = 0;
    Lock lock = new ReentrantLock();

    public int get(){
        return num;
    }

    public void set(){
        lock.lock();
        try {
            num++;
        } finally {
            lock.unlock();
        }
    }*/

    //使用原子类不用加锁

    private AtomicInteger num = new AtomicInteger(0);

    public AtomicInteger get(){
        return num;
    }

    public void set(){
        num.incrementAndGet();
    }

}

/**
 * @className: CASDemo
 * @description: TODO 类描述
 * @author: JJBoomxl
 * @date: 2022/09/08 18:53
 *
 * 多线程环境 不使用    原子类AtomicInteger保证线程安全（基本数据类型）
 *
 *多线程环境   使用    原子类AtomicInteger保证线程安全（基本数据类型）
 */
public class CASDemo {
    public static void main(String[] args) throws InterruptedException {

        CountDownLatch countDownLatch = new CountDownLatch(1000);
        OperatNum operatNum = new OperatNum();

        //测试速度
        long l = System.currentTimeMillis();

        for (int i = 0; i < 1000; i++) {
            new Thread(() -> {
                operatNum.set();
                countDownLatch.countDown();
            },String.valueOf(i)).start();
        }

        long l1 = System.currentTimeMillis();

        countDownLatch.await();

        System.out.println(operatNum.get().get());

        System.out.println("共用时间:" + String.valueOf(l1 - l));
    }

    public static void main1(String[] args) {
        AtomicInteger atomicInteger = new AtomicInteger(5);

        System.out.println(atomicInteger.compareAndSet(5, 25));
        System.out.println(atomicInteger.compareAndSet(5, 30));

    }
}
