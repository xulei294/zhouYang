package com.atguigu.juc;


import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

class SaleTicketDemo{
    private int tickets = 100;

   /*public synchronized void sale(){
       if (tickets > 0){
           String name = Thread.currentThread().getName();
           System.out.println("票号:" + tickets + '\t'+ name + "\t" + "卖出了一张票还剩" + --tickets);
       }
   }*/

    //独占锁+可重入锁+公平|默认非公平锁
    Lock lock = new ReentrantLock(true);


    public void sale(){
        String name = Thread.currentThread().getName();
        lock.lock();
        try {
            if (tickets > 0){
                System.out.println("票号:" + tickets + '\t'+ name + "\t" + "卖出了一张票还剩" + --tickets);
            }
        } finally {
            lock.unlock();
        }
    }



}
/**
 * @className: TicketTreadPool
 * @description: TODO 类描述
 * @author: JJBoomxl
 * @date: 2022/09/08 16:59
 **/
public class TicketTreadPool {

    public static void main(String[] args) {

        ExecutorService service = Executors.newFixedThreadPool(3);
        SaleTicketDemo ticketDemo = new SaleTicketDemo();

        try {
            for (int i = 0; i < 101; i++) {
                service.execute(() -> { ticketDemo.sale(); });
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            service.shutdown();
        }

    }
}
