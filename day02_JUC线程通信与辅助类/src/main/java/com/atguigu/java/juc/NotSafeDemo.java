package com.atguigu.java.juc;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.CopyOnWriteArraySet;

/**
 * @className: NotSafeDemo
 * @description: TODO 类描述
 * @author: JJBoomxl
 * @date: 2022/09/07 19:56
 * 1 故障现象
 * java.util.ConcurrentModificationException
 * <p>
 * 2 导致原因
 * next迭代器，数据不对，总数不匹配modCount != expectedModCount
 * <p>
 * 3 解决方案
 * 3.1 使用线程安全的类，Vector不用
 * 3.2 Collections.synchronizedList不用
 * 3.3 CopyOnWriteArrayList,Yes ,读写分离+写时复制
 * <p>
 * 4 优化建议(同样的错误不犯第2次)
 */
class NotSafeDemo {

    public static void main1(String[] args) {

        List list = new CopyOnWriteArrayList();//Collections.synchronizedList(new ArrayList<>());//new ArrayList();

        for (int i = 0; i < 30; i++) {
            new Thread(() -> {
                list.add(UUID.randomUUID().toString().substring(8));
                System.out.println(list);
            }, String.valueOf(i)).start();
        }
    }

    public static void main2(String[] args) {

        Set set = new CopyOnWriteArraySet();//Collections.synchronizedSet(new HashSet<>());//new HashSet();

        for (int i = 0; i < 30; i++) {
            new Thread(() -> {
                set.add(UUID.randomUUID().toString().substring(8));
                System.out.println(set);
            }, String.valueOf(i)).start();
        }
    }

    public static void main(String[] args) {

        //分段锁技术实现
        Map map = new ConcurrentHashMap();//Collections.synchronizedMap(new HashMap<>());//new HashMap();

        for (int i = 0; i < 30; i++) {
            new Thread(() -> {
                map.put(Thread.currentThread().getName(), UUID.randomUUID().toString().substring(8));
                System.out.println(map);
            }, String.valueOf(i)).start();
        }
    }
}
