package com.atguigu.java.juc;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.TimeUnit;

/**
 * @className: CyclicBarrierDemo
 * @description: 该类是 允许一组线程 互相 等待，直到到达某个公共屏障点，在设计一组固定大小的线程的程序中，这些线程必须
 * 互相等待，因为barrier在释放等待线程后可以重用，所以称为循环barrier
 * <p>
 * 常用的方法有：
 * await() 在所有的参与者都已经在此barrier上调用await方法之前一直等待。
 * @author: JJBoomxl
 * @date: 2022/09/07 22:38
 **/
public class CyclicBarrierDemo {

    public static void main(String[] args) {

        CyclicBarrier cyclicBarrier = new CyclicBarrier(7, () -> {
            System.out.println("集齐七颗龙珠,召唤燕子!!!");
        });

        for (int i = 0; i < 7; i++) {
            try { TimeUnit.SECONDS.sleep(2); } catch (InterruptedException e) { e.printStackTrace(); }

            new Thread(() -> {
                System.out.println(Thread.currentThread().getName() + "\t颗龙珠被收集!!!");
                try {
                    cyclicBarrier.await();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (BrokenBarrierException e) {
                    e.printStackTrace();
                }
            },String.valueOf(i)).start();
        }
    }
}
