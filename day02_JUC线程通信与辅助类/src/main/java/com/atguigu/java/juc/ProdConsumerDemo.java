package com.atguigu.java.juc;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

class AirCondition {
    private int number = 0;


    /*public synchronized void increment() throws InterruptedException {
        //1.判断
        while (number != 0){
            this.wait();
        }
        //2.干活
        System.out.println("线程" + Thread.currentThread().getName() + "\t" + "操作数字变为" + (++number));

        //3.通知
        this.notifyAll();
    }

    public synchronized void decrement() throws InterruptedException {
        //1.判断
        while (number == 0){
            this.wait();
        }
        //2.干活
        System.out.println("线程" + Thread.currentThread().getName() + "\t" + "操作数字变为" + (--number));

        //3.通知
        this.notifyAll();
    }*/

    private Lock lock = new ReentrantLock();
    private Condition condition = lock.newCondition();

    public void increment() throws InterruptedException {
        lock.lock();
        try {
            //1. 判断
            while (number != 0) {
                condition.await();
            }
            //2. 操作
            System.out.println(Thread.currentThread().getName() + "操作了数字\t" + (++number));
            //3.通知
            condition.signalAll();
        } finally {
            lock.unlock();
        }
    }


    public void decrement() throws InterruptedException {

        lock.lock();
        try {
            //1. 判断
            while (number == 0) {
                condition.await();
            }

            //2. 操作
            System.out.println(Thread.currentThread().getName() + "操作了数字\t" + (--number));

            //3.通知
            condition.signalAll();
        } finally {
            lock.unlock();
        }
    }


}

/**
 * @className: ProdConsumerDemo
 * @description: TODO 类描述
 * @author: JJBoomxl
 * @date: 2022/09/07 20:09
 * 有一个初始值为零的变量，现有两个线程对该变量操作，
 * 实现一个线程对变量加1，
 * 实现一个线程对变量减1,
 * 交替来10轮
 * <p>
 * 1 高内聚低耦合前提下，线程   操作      资源类
 * <p>
 * 2 判断、干活、通知
 * <p>
 * 3 防止线程的虚假唤醒
 */
public class ProdConsumerDemo {

    public static void main(String[] args) {

        AirCondition airCondition = new AirCondition();


        new Thread(() -> {
            for (int i = 0; i < 10; i++) {
                try {
                    airCondition.increment();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }, "A").start();

        new Thread(() -> {
            for (int i = 0; i < 10; i++) {
                try {
                    airCondition.decrement();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }, "B").start();

        new Thread(() -> {
            for (int i = 0; i < 10; i++) {
                try {
                    airCondition.increment();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }, "A").start();

        new Thread(() -> {
            for (int i = 0; i < 10; i++) {
                try {
                    airCondition.decrement();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }, "B").start();
    }
}
