package com.atguigu.java.juc;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/**
 * @className: CountDownLatchDemo
 * @description: CountDownLatch 类可以设置一个计数器，然后通过 countDown 方法来进行减 1 的操作，使用 await
 * 方法等待计数器不大于 0，然后继续执行 await 方法之后的语句。具体步骤可以演化为定义一个类，减1操作，并等待到0，
 * 为0执行结果。
 * <p>
 * 两个常用的主要方法
 * await() 使当前线程在锁存器倒计数至零之前一直在等待，除非线程被中断
 * countDown()递减锁存器的计数，如果计数达到零，将释放所有等待的线程
 * @author: JJBoomxl
 * @date: 2022/09/07 21:22
 * CountDownLatch主要有两个方法，当一个或多个线程调用await方法时，这些线程会阻塞。
 * 其它线程调用countDown方法会将计数器减1(调用countDown方法的线程不会阻塞)，
 * 当计数器的值变为0时，因await方法阻塞的线程会被唤醒，继续执行。
 */
public class CountDownLatchDemo {
    public static void main(String[] args) throws InterruptedException {

        CountDownLatch countDownLatch = new CountDownLatch(6);

        for (int i = 0; i < 6; i++) {
            try {
                TimeUnit.SECONDS.sleep(3);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            new Thread(() -> {
                System.out.println(Thread.currentThread().getName() + "\t 离开了自习室");
                countDownLatch.countDown();
            }, String.valueOf(i)).start();
        }


        countDownLatch.await();

        System.out.println("班长离开了自习室,并关上了门");
    }
}
