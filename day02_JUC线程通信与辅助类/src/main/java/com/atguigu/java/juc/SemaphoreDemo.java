package com.atguigu.java.juc;

import java.util.Random;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

/**
 * @className: SemaphoreDemo
 * @description: 一个计数信号量，从概念上将，信号量维护了一个许可集，如有必要，在许可可用前会阻塞每一个acquire()，
 * 然后在获取该许可。每个release()添加一个许可，从而可能释放一个正在阻塞的获取者。但是，不使用实际的许可对象，Semaphore
 * 只对可用许可的号码进行计数，并采取相应的行动
 * <p>
 * 具体常用的方法有：
 * acquire()从此信号量获取一个许可，在提供一个许可前一直将线程阻塞，否则线程被中断
 * release()释放一个许可，将其返回给信号量
 * @author: JJBoomxl
 * @date: 2022/09/07 22:52
 **/
public class SemaphoreDemo {
    public static void main(String[] args) {

        //模拟三个停车位
        Semaphore semaphore = new Semaphore(3);

        //模拟六部汽车
        for (int i = 0; i < 6; i++) {
            new Thread(() -> {
                try {
                    //抢到车位
                    semaphore.acquire();
                    System.out.println(Thread.currentThread().getName() + "抢到了停车位");
                    //模拟停车时间
                    try {
                        TimeUnit.SECONDS.sleep(new Random().nextInt(3));
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    System.out.println(Thread.currentThread().getName() + "离开");

                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    semaphore.release();
                }
            }, String.valueOf(i)).start();
        }
    }
}
