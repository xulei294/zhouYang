package com.atguigu.java.juc;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

class ShareResource {
    private int flag = 1;

    private Lock lock = new ReentrantLock();
    private Condition condition1 = lock.newCondition();
    private Condition condition2 = lock.newCondition();
    private Condition condition3 = lock.newCondition();

    public void print5() throws InterruptedException {
        lock.lock();
        try {
            //1.判断
            while (flag != 1) {
                condition1.await();
            }
            //2.执行
            for (int i = 0; i < 5; i++) {
                System.out.println(Thread.currentThread().getName() + ":\t" + "AA");
            }

            //3.通知
            flag = 2;
            condition2.signal();
        } finally {
            lock.unlock();
        }
    }

    public void print10() throws InterruptedException {
        lock.lock();
        try {
            //1.判断
            while (flag != 2) {
                condition2.await();
            }
            //2.执行
            for (int i = 0; i < 10; i++) {
                System.out.println(Thread.currentThread().getName() + ":\t" + "BB");
            }

            //3.通知
            flag = 3;
            condition3.signal();
        } finally {
            lock.unlock();
        }
    }

    public void print15() throws InterruptedException {
        lock.lock();
        try {
            //1.判断
            while (flag != 3) {
                condition3.await();
            }
            //2.执行
            for (int i = 0; i < 15; i++) {
                System.out.println(Thread.currentThread().getName() + ":\t" + "CC");
            }

            //3.通知
            flag = 1;
            condition1.signal();
        } finally {
            lock.unlock();
        }
    }
}

/**
 * @className: ThreadOrderAccess
 * @description: TODO 类描述
 * @author: JJBoomxl
 * @date: 2022/09/07 20:51
 * 多线程之间按顺序调用，实现A->B->C
 * 三个线程启动，要求如下：
 * <p>
 * AA打印5次，BB打印10次，CC打印15次
 * 接着
 * AA打印5次，BB打印10次，CC打印15次
 * ......来10轮
 * <p>
 * 1 线程 操作  资源类
 * <p>
 * 2 判断 干活 通知
 * <p>
 * 3 防止虚假唤醒
 * <p>
 * 4 多线程的标志位变更
 */
public class ThreadOrderAccess {

    public static void main(String[] args) {

        ShareResource shareResource = new ShareResource();

        new Thread(() -> {
            try {
                for (int i = 0; i < 10; i++) {
                    shareResource.print5();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }, "线程1").start();

        new Thread(() -> {
            try {
                for (int i = 0; i < 10; i++) {
                    shareResource.print10();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }, "线程2").start();

        new Thread(() -> {
            try {
                for (int i = 0; i < 10; i++) {
                    shareResource.print15();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }, "线程3").start();
    }
}
