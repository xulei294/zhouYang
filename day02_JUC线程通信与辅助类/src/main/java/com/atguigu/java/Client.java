package com.atguigu.java;

import java.util.HashSet;

/**
 * @className: Client
 * @description: 测试Hash碰撞
 * @author: JJBoomxl
 * @date: 2022/09/07 19:23
 **/
public class Client {
    public static void main(String[] args) {
        HashSet hashSet = new HashSet();
        int myHashCode = 0;

        for (int i = 0; i < 120000; i++) {

            myHashCode = new Object().hashCode();

            if (hashSet.contains(myHashCode)) {
                System.out.println("---hash冲突发生,在第" + i + "/hash值为" + myHashCode);
                continue;
            }

            hashSet.add(myHashCode);
        }


        System.out.println(hashSet.size());
    }
}
